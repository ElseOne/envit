const fs = require('fs')
const path = require('path')

/**
 * Simple class to handle env file configs
 */
class Envfile {

  constructor(envFile, varPrefix = '') {
    this.filepath = path.join(process.cwd(), envFile)
    this.varPrefix = varPrefix
    this.parsed = this._parse(this.filepath)
  }

  /**
   * Returns the object parsed contents of the envfile specified
   * if the file does not exist returns an empty object
   */
  _parse(envFilePath) {

    //returning empty obj if not exiting config file
    if( !fs.existsSync(envFilePath) ){
      return {}
    }

    const envFileExt = path.extname(envFilePath).toLowerCase(),
      envFileContents = this._read(envFilePath)

    let parsed

    if (envFileExt === '.json' || envFileExt === '.js') {
      parsed = require(envFilePath)
    } else {
      parsed = this._parseContents(envFileContents)
    }

    return parsed

  }

  /**
   * Returns the contents of the env file specified
   * throws an error if the file does not exists
   */
  _read(envFilePath) {

    // Attempt to open the provided file
    let file
    try {
      file = fs.readFileSync(envFilePath, {encoding: 'utf8'})
    } catch (err) {
      throw new Error(err)
    }

    return file;

  }

  /**
   * Returns the parsed config from the raw contents provided
   */
  _parseContents(rawContents){

    let rawString = rawContents.toString()
    rawString = this._stripComments(rawString)
    rawString = this._stripEmptyLines(rawString)

    const envs = this._parseVars(rawString)

    return envs

  }

  /**
   * Strips out the comments from the raw string provided
   */
  _stripComments(envString){
    const commentsRegex = /(^#.*$)/gim
    let match = commentsRegex.exec(envString)
    let newString = envString
    while (match != null) {
      newString = newString.replace(match[1], '')
      match = commentsRegex.exec(envString)
    }
    return newString
  }

  /**
   * Strips out the empty lines from the raw string provided
   */
  _stripEmptyLines(envString){
    const emptyLinesRegex = /(^\n)/gim
    return envString.replace(emptyLinesRegex, '')
  }

  /**
   * Tranforms the previously cleaned content string provided into an object
   */
  _parseVars(envString){
    const envParseRegex = /^((.+?)[=](.*))$/gim
    const matches = {}
    let match
    while ((match = envParseRegex.exec(envString)) !== null) {
      // Note: match[1] is the full env=var line
      matches[this.varPrefix + match[2]] = match[3]
    }
    return matches
  }

}

module.exports = Envfile