const spawn = require('cross-spawn').spawn
const fs = require('fs')
const path = require('path')
const Envfile = require('./envfile')

/**
 * Main class for env files handling
 */
class Envit {

  /**
   * Instead of the instance returns the childprocess charged with env variables
   */
  constructor(args) {
    //default options
    this.options = this._getOptions()
    //original args
    this._processArgs = args
    //parsed args
    this.parsedArgs = this._parseArgs()
    //envs collection
    this.envs = this._getEnvs()
    //merged envs
    const env = Object.assign.apply(Object, [
      {}, process.env, ...this.envs
    ])
    // Execute the command with the given environment variables
    this.childProcess = spawn(this.parsedArgs.command, this.parsedArgs.commandArgs, {
      stdio: 'inherit',
      env
    })
    //process/childProcess communication
    process.on('SIGTERM', this.childProcess.kill.bind(this.childProcess, 'SIGTERM'))
    this.childProcess.on('exit', process.exit)
    //returning childProcess
    return this.childProcess
  }

  /**
   * Mapping of the possible envit config options
   */
  get optionsMap() {
    return {
      envFile: {
        cli: '--env-file',
        default: '.env'
      },
      envFileBasepath: {
        cli: '--env-file-basepath',
        default: './'
      },
      envTarget: {
        cli: '--env-target',
        default: 'development'
      },
      varPrefix: {
        cli: '--var-prefix',
        default: ''
      }
    }
  }

  /**
   * Returns an object containing options default values only
   */
  get defaultOptions() {
    const optionsMap = this.optionsMap
    return Object.keys(optionsMap).reduce((defaults, option) => {
      defaults[option] = optionsMap[option].default
      return defaults
    }, {})
  }

  /**
   * Returns envit config options which will be
   * default options merged with envitrc if found
   * else default options
   */
  _getOptions() {

    const rcFile = path.join(process.cwd(), '.envitrc')

    if (fs.existsSync(rcFile)) {
      let rcOptions = JSON.parse(fs.readFileSync(rcFile))
      return Object.assign({}, this.defaultOptions, rcOptions)
    }

    return this.defaultOptions

  }

  /**
   * Detects if a single passed argument is an actual envit configuration
   */
  _isCliOption(arg) {
    var itIs = false
    var optionTests = this.optionsMap
    for (let option in optionTests) {
      let optionRegex = new RegExp(`^(${optionTests[option].cli}[=](.*))$`)
      if (optionRegex.test(arg)) {
        let optionMatches = optionRegex.exec(arg)
        this.options[option] = optionMatches[2]
        itIs = true
        break
      }
    }
    return itIs
  }

  /**
   * Parses the arguments cli-provided
   * assumes the first is the env file path
   */
  _parseArgs() {

    let args = this._processArgs.slice()

    if (args.length < 2) {
      throw new Error('Error! Too few arguments passed')
    }

    let command
    let commandArgs = args.slice()

    while (commandArgs.length) {
      const arg = commandArgs.shift()
      if (!this._isCliOption(arg)) {
        command = arg
        break
      }
    }

    return {command, commandArgs}

  }

  /**
   * Returns array of parsed env objs
   */
  _getEnvs() {

    let envFilePath = this.options.envFileBasepath + this.options.envFile,
      envFiles = [
        //base
        envFilePath,
        //build target overrides
        `${envFilePath}.${this.options.envTarget}`
      ].filter(Boolean).map(envFile => this._getEnv(envFile), this)

    return envFiles

  }

  /**
   * Returns a single parsed env obj
   */
  _getEnv(envFile) {
    return new Envfile(envFile, this.options.varPrefix).parsed
  }

}

module.exports = Envit