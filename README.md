#envit

A simple node cli made to inject env vars into commands using an env file


##Usage

You can use a .env file placed at the root of your project.

Accepted file formats are:
* no extension ( example: /.env )
* js extension ( example: /env.js ): the code contained have to export an object containing the env vars in a key: value fashion
* json extension ( example: /env.json ): json description of the env vars

By default an override file postfixed with '.development' will be searched an, if found, its contents will extend and override the env file provided.

You can add the .env.development file to gitignore and use it only during development phase.

### String syntax
Files without js or json extension will be parsed as simple strings.
The syntax to be used to properly define vars in them is:

```
# This is a comment and will be skipped
ENV1=THIS IS A VAR
ENV2=ANOTHER VAR WHICH IS ACTUALLY A VERY VERY LONG STRING VARIABLE

ENV3=NOTICE THAT YOU CAN SAFELY USE BLANK LINES
```

##Advanced usage

You can optionally specify:

* a relative path name to your env file
* a relative base path where your env file will be found
* an env target to be used instead of '.development' override env
* a prefix to be applied to injected env vars

You can specify these options either in the package.json scripts

```
...
"scripts": {
    "start": "envit --env-file=base.env --env-file-basepath=./config/ --env-target=production --var-prefix=prod webpack"
}
...
```

or using a .envitrc file containing a json with the same configurations

```
{
  "envFile": "base.env",
  "envFileBasepath": "./config/",
  "envTarget": "production",  
  "varPrefix": "prod"
}
```

Since the cli options get passed to envit after the .envitrc options, your can use use the .envitrc file for your custom defaults, overriding them with the cli approach.
